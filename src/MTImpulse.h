// MTImpulse.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.
//
// Description:
// Represents an impulse signal, outputs zeroes by default, 1.0 when told to.
//

#ifndef MTIMPULSE_H_
#define MTIMPULSE_H_

#include <stk/Generator.h>
#include <stdexcept>

namespace stk {

class MTImpulse : public Generator {
    public:
        // Constructor
        MTImpulse() : nextOut_(1.0), gain_(1.0), muted_(false) {}

        void setGain(StkFloat gain) { gain_ = 10 * gain; }

        void setMute(bool muted) { muted_ = muted; }

        // When an impulse is muted, it's effectively inactive, but
        // we want to remember it's gain.
        bool isMuted() const { return muted_; }

        // When a ticker's gain is too low, we don't want
        // to use it since it could produce CPU spikes
        // during filtering (see http://en.wikipedia.org/wiki/Denormal)
        bool isSilent() const { return gain_ <= 1e-9; }

        // Set next output sample to peak
        void excite();

        StkFloat tick();
        StkFrames& tick(StkFrames &frames, unsigned int /*channel*/);

    private:
        StkFloat nextOut_;
        StkFloat gain_;
        bool muted_;
};

inline void MTImpulse::excite()
{
    nextOut_ = gain_;
}

inline StkFloat MTImpulse::tick()
{
    lastFrame_[0] = nextOut_;
    nextOut_ = 0.0;
    return lastFrame_[0];
}

inline StkFrames& MTImpulse::tick(StkFrames& frames, unsigned int /*channel*/)
{
    throw std::logic_error("unimplemented");
    return frames;
}
}  // namespace stk

#endif // MTIMPULSE_H_
