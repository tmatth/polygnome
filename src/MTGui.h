// MTGui.h
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// based on:
// MPI_GuiRootWindow.h
// by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MT_GUI_H_
#define MT_GUI_H_

#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/volumebutton.h>
#include <memory>

#include "MTBeatWidget.h"
#include "MTMeterMapping.h"
#include "MTMuteButton.h"
#include "MTEventFwd.h"
#include "ringbuffer.h"
#include "noncopyable.h"

namespace stk {
    class MTAudio;
}
class MTBeatEvent;

class MTGui : public Gtk::Window
{
    public:
        NON_COPYABLE(MTGui);
        MTGui(stk::MTAudio &audio);
        virtual ~MTGui();
    private:
        void sendEvent(const MTEvent::Event &event);
        void loadDefaults();
        void saveSettings(const std::string &settingsPath) const;
        bool loadSettings(const std::string &settingsPath);
        void createWidgets();
        bool onKeyPressEvent(Gtk::Widget *widget, GdkEventKey *event);
        void onStartChanged();
        void onBpmChanged();
        void onFreqChanged(int index);
        void onMeterChanged(int index);
        void onGainChanged(double val, int index);
        void onMuteChanged(int index);
        void onMasterClicked(int index);
        static void redrawWidget(GdkWindow *window);
        bool pollMessages();
        void createFileMenu();
        void onMenuOpen();
        void onMenuSave();
        void onMenuQuit();
        void onMenuAbout();
        stk::MTAudio &audio_;

        // widgets
        Gtk::VBox vbox_;
        Gtk::ToggleButton startButton_;
        Gtk::SpinButton bpmButton_;
        Gtk::SpinButton freqButtons_[MTMeterMapping::NUM_TICKERS];
        Gtk::SpinButton meterButtons_[MTMeterMapping::NUM_TICKERS];
        Gtk::VolumeButton gainButtons_[MTMeterMapping::NUM_TICKERS];
        Gtk::RadioButton masterButtons_[MTMeterMapping::NUM_TICKERS];
        MTMuteButton muteButtons_[MTMeterMapping::NUM_TICKERS];
        MTBeatWidget beatWidgets_[MTMeterMapping::NUM_TICKERS];
        std::shared_ptr<Gtk::AboutDialog> about_;
        double masterMeter_;
};

#endif // MTGUI_H_
