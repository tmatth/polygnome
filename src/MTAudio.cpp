// MTAudio.cpp
//
// Copyright (c) 2008-2013 Tristan Matthews <le.businessman@gmail.com>
//               2010 Alexandre Quessy <alexandre@quessy.net>
//
// based on:
// MPI_Audio.cpp
// by Mark Zadel
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <cstring>
#include <unistd.h>
#include "MTAudio.h"
#include "MTTicker.h"
#include "MTMeterMapping.h"

using namespace stk;

namespace {
    const int CHANNELS = 2;
    const int BUFFER_SIZE = 1024;
}

MTAudio *MTAudio::create()
{
    Stk::setSampleRate(44100.0);
    Stk::showWarnings(true);
    return new MTAudio;
}

MTAudio::MTAudio() :
    outgoingEvents_(BUFFER_SIZE),
    incomingEvents_(BUFFER_SIZE),
    quitFlag_(false),
    dac_(),
    tickers_()
{
    // create tickers
    for (size_t i = 0; i != MTMeterMapping::NUM_TICKERS; ++i) {
        std::shared_ptr<MTTicker> ticker(new MTTicker(outgoingEvents_, i));
        tickers_.push_back(ticker);
    }

}

namespace {
    bool handleJackSampleRateError(const std::string &errorText)
    {
        // here we see if the problem was simply a sample rate mismatch,
        // in which case, we adjust our sample rate to match
        const std::string rateString("is different than the JACK server rate (");
        size_t startStr = errorText.find(rateString);
        if (startStr != std::string::npos) {
            startStr += rateString.length();
            size_t end = errorText.find_last_of(")");
            std::stringstream ss;
            ss << errorText.substr(startStr, end - startStr);
            unsigned int rate = 0;
            ss >> rate;
            std::cerr << "Trying new sample rate " << rate << std::endl;
            Stk::setSampleRate(rate);
            return true;
        } else {
            return false;
        }
    }
} // end anonymous namespace

void MTAudio::start()
{
    using std::shared_ptr;
    RtAudio::StreamParameters parameters;
    parameters.deviceId = dac_.getDefaultOutputDevice();
    parameters.nChannels = CHANNELS;
    RtAudioFormat format = (sizeof(StkFloat) == 8) ? RTAUDIO_FLOAT64 :
        RTAUDIO_FLOAT32;
    //RtAudioFormat format = RTAUDIO_SINT32;
    RtAudio::StreamOptions options;
    options.flags |= RTAUDIO_SCHEDULE_REALTIME;
    options.flags |= RTAUDIO_ALSA_USE_DEFAULT;
    options.streamName = PACKAGE_NAME; // jack-only option

    unsigned int bufferFrames = RT_BUFFER_SIZE;
    const int MAX_ATTEMPTS = 3;
    int attempts = 0;
    do {
        if (dac_.openStream(&parameters, NULL, format,
                            static_cast<unsigned int>(Stk::sampleRate()),
                            &bufferFrames,
                            &tickBufferCb,
                            this,
                            &options))
        {
            std::cerr << dac_.getErrorText() << std::endl;
            // here we see if the problem was simply a sample rate mismatch,
            // in which case, we adjust our sample rate to match
            if (not handleJackSampleRateError(dac_.getErrorText()))
                usleep(100000); // 100 milliseconds

            if (attempts == MAX_ATTEMPTS) {
                std::cerr << "ERROR: Unable to start audio backend"
                    << std::endl;
                break;
            }
        }
        ++attempts;
    } while (not dac_.isStreamOpen());

    if (dac_.startStream())
       std::cerr << dac_.getErrorText() << std::endl;
}

MTAudio::~MTAudio()
{
    if (dac_.isStreamRunning()) {
        if (dac_.stopStream())
            std::cerr << dac_.getErrorText() << std::endl;
    }

    if (dac_.isStreamOpen())
        dac_.closeStream();
}

int MTAudio::tickBufferCb(void *out, void * /*in*/,
        unsigned int bufferSz,
        double /*streamTime*/,
        RtAudioStreamStatus /*status*/,
        void * data)
{
    MTAudio *context = static_cast<MTAudio*>(data);
    return context->tickBuffer(out, bufferSz);
}

int MTAudio::tickBuffer(void *out, unsigned int bufferSz)
{
    using std::shared_ptr;
    using std::vector;

    // process control messages from the main thread
    // we don't want this loop to take too long
    const int MAX_EVENTS = 50;
    for (int i = 0; incomingEvents_.can_read() and i < MAX_EVENTS; ++i) {
        // grab an event and execute it
        incomingEvents_.read()();
        if (quitFlag_)
            return 1;
    }

    // the buffer contains stereo interleaved samples, so clear out the whole
    // thing
    try {
        memset(out, 0x0, CHANNELS * bufferSz * sizeof(StkFloat));

        // for each ticker, add in its contribution
        const auto& tv = tickers_;
        for (auto&& t : tv) {

            if (t->isActive()) {
                StkFloat *curbufpos = static_cast<StkFloat*>(out);
                for (unsigned i = 0; i < bufferSz; ++i) {
                    const StkFloat tickvalue = t->tick();
                    // output is stereo and interleaved
                    for (int c = 0; c < CHANNELS; ++c) {
                        *curbufpos += tickvalue;
                        ++curbufpos;
                    }
                }
            }
        }
    } catch (StkError &error) {
        error.printMessage();
        return 1;
    }

    return 0;
}

std::vector<std::shared_ptr<stk::MTTicker> >&
MTAudio::getTickers()
{
    return tickers_;
}
