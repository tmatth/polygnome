//
// Copyright (c) 2011 Florian Paul Schmidt <mista.tapas@gmx.net>
//               2008-2013 Tristan Matthews <le.businessman@gmail.com>
//
// This file is part of Polygnome.
//
// Polygnome is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Polygnome is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Polygnome.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#define USE_MLOCK
#include <jack/ringbuffer.h>
#include <stdexcept>
#include "noncopyable.h"

/**
    T needs to be a default constructable type.. And it has to have valid
    copy constructor/assignment operator

    Note that this class needs to construct n objects of type T (with n == size)  so
    that the places in the ringbuffer become assignable

    Note that the objects remain in the ringbuffer until they are overwritten again
    when the ringbuffer returns to the current position the next time around. I.e. a
    read() does not assign a T() to the read object as that could cause destructors
    to be called, etc..

    Note that read() creates a copy of the T, so the T(const T&) should be non blocking
*/
template <class T>
class RingBuffer {
    private:
        size_t size_;
        jack_ringbuffer_t *ringbuffer_;

    public:

    explicit RingBuffer(size_t size) :
        size_(size), ringbuffer_(jack_ringbuffer_create(sizeof(T) * size_))
    {
        // try to lock memory
        if (jack_ringbuffer_mlock(ringbuffer_))
            throw std::runtime_error("Error on call to jack_ringbuffer_mlock");

        for (size_t i = 0; i < size_; ++i) {
            // using placement new, don't actually need the pointer
            T *t = new (ringbuffer_->buf + sizeof(T) * i) T;
            (void) t;
        }
    }

    ~RingBuffer()
    {
        for (size_t i = 0; i < size_; ++i)
            reinterpret_cast<T*>(ringbuffer_->buf + sizeof(T) * i)->~T();
        jack_ringbuffer_free(ringbuffer_);
    }

    bool can_write() const
    {
        return jack_ringbuffer_write_space(ringbuffer_) >= sizeof(T);
    }

    void write(const T &t)
    {
        jack_ringbuffer_data_t rb_data[2];
        jack_ringbuffer_get_write_vector(ringbuffer_, rb_data);
        *(reinterpret_cast<T*>(rb_data->buf)) = t;
        jack_ringbuffer_write_advance(ringbuffer_, sizeof(T));
    }

    bool can_read() const
    {
        return jack_ringbuffer_read_space(ringbuffer_) >= sizeof(T);
    }

    T& read()
    {
        jack_ringbuffer_data_t rb_data[2];
        jack_ringbuffer_get_read_vector(ringbuffer_, rb_data);
        jack_ringbuffer_read_advance(ringbuffer_, sizeof(T));
        return *(reinterpret_cast<T*>(rb_data->buf));
    }

    NON_COPYABLE(RingBuffer);
};

#endif // RINGBUFFER_H_
