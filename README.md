# Polygnome

The polymetric metronome

### Prerequisites

Debian-based distros:
```
# skip autoconf if building from a tarball
apt-get install build-essential autoconf libstk0-dev libgtkmm-3.0-dev
```

### Building


```
./autogen.sh
./configure
make
make install
```

### Running the tests

```
make check
```

### License

This project is licensed under the GPL v3 (or any later version at your option) - see the [COPYING](COPYING) file for details

### Acknowledgments

* The idea to pass a closure into the realtime thread via a JACK ringbuffer came from a post by Florian Schmidt
* Inspiration for the organization of the code was heavily inspired by [Different Srokes](http://www.music.mcgill.ca/~zadel/differentstrokes/) by Mark Zadel
