#include <iostream>
#include <cmath>
#include <array>
#include <stk/Stk.h>
#include "MTAudio.h"
#include "MTMeterMapping.h"
#include "MTTicker.h"
#include "test_data.h"

/* Scale the data and floor it so that we can do a comparison */
int scaleUp(double f)
{
    return floor(10000.0 * f);
}

int main()
{
    using namespace stk;
    stk::MTAudio audio;
    const unsigned CHANNELS = 2;
    const unsigned nFrames = sizeof(AUDIO_TEST_DATA) / sizeof(AUDIO_TEST_DATA[0]);
    // One buffer to receive stereo, interleaved samples
    std::array<stk::StkFloat, nFrames * CHANNELS> out;
    for (size_t i = 0; i < MTMeterMapping::NUM_TICKERS; ++i) {
        MTTicker &t = *audio.getTickers()[i];
        t.setActiveFlag(true);
        t.bpm(60);
        t.freq(440.0);
    }

    audio.tickBuffer(out.data(), out.size() / CHANNELS);
    for (unsigned i = 0; i < nFrames; i += 2) {
        if (scaleUp(out[i]) != AUDIO_TEST_DATA[i]) {
             std::cerr << "FAIL: Expected " << AUDIO_TEST_DATA[i] << ", got " <<
                 scaleUp(out[i]) << " in left channel!" << std::endl;
             return 1;
        } else if (scaleUp(out[i + 1]) != AUDIO_TEST_DATA[i]) {
             std::cerr << "FAIL: Expected " << AUDIO_TEST_DATA[i] << ", got " <<
                 scaleUp(out[i + 1]) << " in right channel!" << std::endl;
             return 1;
        }
    }
    return 0;
}
