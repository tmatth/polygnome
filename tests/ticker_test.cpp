#include <iostream>
#include <cassert>
#include <cmath>
#include "MTTicker.h"
#include "test_data.h"

/* Scale the data and floor it so that we can do a comparison */
int scaleUp(double f)
{
    return floor(10000.0 * f);
}

int main()
{
    using namespace stk;
    RingBuffer<MTBeatEvent> outgoingEvents(1024);
    MTTicker t(outgoingEvents, 1);
    t.setActiveFlag(true);
    t.bpm(60);
    t.freq(440.0);
    for (size_t i = 0; i < sizeof(TEST_DATA) / sizeof TEST_DATA[0]; ++i) {
         if (scaleUp(t.tick()) != TEST_DATA[i]) {
             std::cerr << "FAIL: Ticker produced unexpected value!";
             return 1;
         }
    }

    return 0;
}
